import { Formatter } from '../interfaces/formatter.js'

export class Payment implements Formatter {
  constructor(
    private recipient: string,
    private details: string,
    private amount: number
  ) { }

  format() {
    return `${this.recipient} id owed $${this.amount} for ${this.details}`;
  }
}
