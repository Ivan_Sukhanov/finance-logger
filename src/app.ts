import { Invoice } from './classes/Invoice.js'
import { Payment } from './classes/Payment.js'
import { Formatter } from './interfaces/formatter.js'
import { ListTemplate } from './classes/ListTemplate.js'

// form
const form = document.querySelector('.form') as HTMLFormElement;

// inputs
const type = document.getElementById('type') as HTMLSelectElement;
const toFrom = document.getElementById('tofrom') as HTMLInputElement;
const details = document.getElementById('details') as HTMLInputElement;
const amount = document.getElementById('amount') as HTMLInputElement;

const ul = document.querySelector('ul')!;
const list = new ListTemplate(ul)

form.addEventListener('submit', (e: Event) => {
  e.preventDefault();

  let doc: Formatter;

  if (type.value === 'invoice') {
    doc = new Invoice(toFrom.value, details.value, amount.valueAsNumber)
  } else {
    doc = new Payment(toFrom.value, details.value, amount.valueAsNumber)
  }

  list.render(doc, type.value, 'end')
})
